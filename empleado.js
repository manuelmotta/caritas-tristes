function Empleado(primerNombre, primerApellido){
    this.primerNombre = primerNombre;
    this.segundoNombre;
    this.primerApellido = primerApellido;
    this.segundoApellido;
    this.numeroIdentificacion;
    this.tipoIdentificacion;
    console.log("Empleado: " + this.primerApellido + ' ' + this.primerNombre + " instanciado.");
}



function test(){
    var oEmpleado = new Empleado("Manuel", "Motta");
    console.log("oEmpleado es: " + oEmpleado.primerNombre + " " + oEmpleado.primerApellido);
}